package cancellationReport;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class EpaisaLoginPage {
	static WebDriver driver;
	// static String todaysDate = "6 August 2019";
	static String username = "765825246";
	static String password = "Bounce@123";

	static String testData = "";

	public static void ePaisaLogin() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");

		ChromeOptions options = new ChromeOptions();

		options.addArguments("disable-notifications");
		driver = new ChromeDriver(options);

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://10.44.0.68:8080/bajaj/login.jsp");

		csvWriter objcsvWriter = new csvWriter();
		testData = objcsvWriter.readLoginDetails("ePaisa");

		String[] loginDetails = testData.split(" ");

		username = loginDetails[0];
		password = loginDetails[1];

		driver.findElement(By.xpath("//div//input[@id='userName']")).sendKeys(username);
		Thread.sleep(1000);

		driver.findElement(By.xpath("//div//input[@id='passWord']")).sendKeys(password);
		Thread.sleep(1000);

		driver.findElement(By.xpath("//div[@class='loginbutton']/input")).click();

		Thread.sleep(5000);

		Alert alert = driver.switchTo().alert();
		alert.accept();

		
	}

	public static void ClickOnReports() throws Exception {
		driver.findElement(By.xpath("//div[@class='hmenu']/ul/li/a[text()='Reports']")).click();
		Thread.sleep(1000);
	}

	public static void ClickOnCancellationReports() throws InterruptedException {
		driver.findElement(
				By.xpath("//div[@id='paymenttableRight']/div[@id='topgapNew1']/a[text()='Cancellation Report ']"))
				.click();
		Thread.sleep(1000);
	}

	public static void SelectFromDate(String date) throws Exception {
		// Click on date picker
		driver.findElement(
				By.xpath("(//span[@class='bigbluetxttable']/input[@id='reportStartDate']/following::img)[1]")).click();
		Thread.sleep(1000);

		String[] str = date.split(" ");

		Thread.sleep(2000);
		By month = By.xpath("//span[@class='ui-datepicker-month']");
		By year = By.xpath("//span[@class='ui-datepicker-year']");

		String monthYear = str[1] + str[2];

		By prevArrow = By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-w'][text()='Prev']");

		while (!monthYear.equals("")) {

			// getMonth
			String getMonth = driver.findElement(month).getText();
			// getYear
			String getYear = driver.findElement(year).getText();

			String getMonthYear = getMonth + getYear;

			// select month
			if (monthYear.equals(getMonthYear.trim())) {

				Thread.sleep(1000);

				break;

			} else {
				driver.findElement(prevArrow).click();
			}
		}

		// select date
		List<WebElement> date1 = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//tr/td/a"));

		for (WebElement webElement : date1) {
			// System.out.println(webElement.getText());
			if (webElement.getText().equals(str[0])) {
				Thread.sleep(1000);
				webElement.click();

			}

		}
	}

	public static void ClickOnDownload() throws Exception {
		// click on download
		driver.findElement(By.xpath("//img[@id='reportImageCsv']")).click();
		Thread.sleep(10000);
	}

	public static void Logout() {
		// click on Logout
		driver.findElement(By.xpath("//div[@class='loginDetails']//a[text()='Logout']")).click();

		driver.close();
	}
	
	
	
	
	public static void SelectToDate(String date) throws Exception {
		// Click on date picker
		driver.findElement(
				By.xpath("(//span[@class='bigbluetxttable']/input[@id='reportStartDate']/following::img)[2]")).click();
		Thread.sleep(1000);

		String[] str = date.split(" ");

		Thread.sleep(2000);
		By month = By.xpath("//span[@class='ui-datepicker-month']");
		By year = By.xpath("//span[@class='ui-datepicker-year']");

		String monthYear = str[1] + str[2];

		By prevArrow = By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-w'][text()='Prev']");

		while (!monthYear.equals("")) {

			// getMonth
			String getMonth = driver.findElement(month).getText();
			// getYear
			String getYear = driver.findElement(year).getText();

			String getMonthYear = getMonth + getYear;

			// select month
			if (monthYear.equals(getMonthYear.trim())) {

				Thread.sleep(1000);

				break;

			} else {
				driver.findElement(prevArrow).click();
			}
		}

		// select date
		List<WebElement> date1 = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//tr/td/a"));

		for (WebElement webElement : date1) {
			// System.out.println(webElement.getText());
			if (webElement.getText().equals(str[0])) {
				Thread.sleep(1000);
				webElement.click();

			}

		}
	}

	
	
	
	
	

}
