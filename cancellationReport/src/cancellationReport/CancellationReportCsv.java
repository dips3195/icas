package cancellationReport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.opencsv.CSVReader;

public class CancellationReportCsv {

	public static void main(String[] args) throws Exception {
		CancellationReportCsv obj = new CancellationReportCsv();
		// obj.UnmappedReceiptDataCsvFile("C0000881900171", "23604", "IGNORED",
		// "28-08-2019");
		 obj.Cancellation_Report_csv();
		int con = obj.getCounteOfValidData();
		System.out.println(con);
	}

	public static void Cancellation_Report_csv() {

		String csvFile = System.getProperty("user.home") + "\\Downloads\\Cancellation_Report.csv";

		int counter = 0;
		csvWriter objcsvWriter = new csvWriter();

		try {

			FileReader filereader = new FileReader(csvFile);

			CSVReader csvReader = new CSVReader(filereader);

			String[] nextRecord;
			String[] strArray = null;
			int i = 0;
			String CHEQUE_ID = null;
			String PROPOSAL_NO = "";
			String RECEIPT_NO = "";
			String RECEIPT_AMOUNT = "";
			String CHANNEL_CODE = "";
			String RECON_ALLOWED = "";
			String CANCELLATION_DATE = "";
             String STATE="";
             String BRANCH_NAME="";
			// objcsvWriter.cleanCSV();

			while ((nextRecord = csvReader.readNext()) != null) {
				i++;
				if (i > 2) {

					strArray = nextRecord[0].replace("|", ",").split(",");

					 
					RECEIPT_NO = strArray[5];
				    STATE = strArray[16];
					CHANNEL_CODE = strArray[11];
					RECON_ALLOWED = "IGNORED";
					BRANCH_NAME = strArray[15];
					CANCELLATION_DATE = strArray[18];
					PROPOSAL_NO = strArray[2];
					RECEIPT_AMOUNT = strArray[7];
					System.out.println(BRANCH_NAME);
					 System.out.println(STATE);
					
					CHEQUE_ID = UnmappedReceiptDataCsvFile(RECEIPT_NO, CHANNEL_CODE, RECON_ALLOWED, CANCELLATION_DATE,BRANCH_NAME,STATE);

					if (CHEQUE_ID.equals("")) {

						objcsvWriter.csvWriterForNoDataPresent(PROPOSAL_NO, RECEIPT_NO, RECEIPT_AMOUNT,
								CHANNEL_CODE, RECON_ALLOWED, CANCELLATION_DATE,BRANCH_NAME,STATE);
					}

					CHEQUE_ID = "";
					 PROPOSAL_NO = "";
					 BRANCH_NAME = "";
					  STATE = "";
					CHANNEL_CODE = "";
					RECON_ALLOWED = "";
					CANCELLATION_DATE = "";
					RECEIPT_AMOUNT="";
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("CSV Data successfully written");

	}

	public static String UnmappedReceiptDataCsvFile(String RECEIPT_NO, String CHANNEL_CODE, String RECON_ALLOWED,
			String CANCELLATION_DATE,String BRANCH_NAME,String STATE) {

		String CHEQUE_ID = "";
		String PROPOSAL_NO = "";
		// String BRANCH_NAME = "";
		String RECEIPT_AMOUNT = "";
      //  String STATE="";
		csvWriter objcsvWriter = new csvWriter();
		File file = new File(System.getProperty("user.home") + "\\Downloads");
		String fileName = null;
		File[] filelist = file.listFiles();

		for (File file2 : filelist) {

			if (file2.getName().trim().contains("UNMAPPED_RECEIPT_DATA")) {
				fileName = file2.getName();
				// System.out.println("fileName===> " + fileName);
			}

		}

		try {

			String csvFile2 = System.getProperty("user.home") + "\\Downloads\\" + fileName;

			String[] nextRecord2;
			int j = 0;

			FileReader filereader2 = new FileReader(csvFile2);
			CSVReader csvReader2 = new CSVReader(filereader2);

			while ((nextRecord2 = csvReader2.readNext()) != null) {
				j++;
				// CHEQUE_ID = "";
				if (j > 1) {
					if (RECEIPT_NO.equals(nextRecord2[4])) {

						PROPOSAL_NO = nextRecord2[1];
						CHEQUE_ID = nextRecord2[2];
						RECEIPT_NO = nextRecord2[4];
						RECEIPT_AMOUNT = nextRecord2[6];
						
						// System.out.println(PROPOSAL_NO);
						// System.out.println(CHEQUE_ID);
						// System.out.println(RECEIPT_NO);
						// System.out.println(RECEIPT_AMOUNT);

						// System.out.println("===========================================");
						objcsvWriter.csvWriterForPresenceOfData(CHEQUE_ID, PROPOSAL_NO, RECEIPT_NO, RECEIPT_AMOUNT,
								CHANNEL_CODE, RECON_ALLOWED, CANCELLATION_DATE,BRANCH_NAME,STATE);

					}

					PROPOSAL_NO = "";
					// RECEIPT_NO = "";
					RECEIPT_AMOUNT = "";
					// CHEQUE_ID = "";

				}

			}

		} catch (

		Exception e) {
			e.printStackTrace();

		}
		return CHEQUE_ID;
	}

	public static int getCounteOfValidData() throws Exception {
		csvWriter objcsvWriter = new csvWriter();

		String date = objcsvWriter.getCurrentDate();

		String outputFile = System.getProperty("user.dir") + "\\Output\\Valid_Data_File_" + date + ".csv";

		FileReader filereader2 = new FileReader(outputFile);
		CSVReader csvReader2 = new CSVReader(filereader2);
		String[] nextRecord;
		int counter = 0;

		while ((nextRecord = csvReader2.readNext()) != null) {
			counter++;

		}
		return counter - 1;
	}

}
