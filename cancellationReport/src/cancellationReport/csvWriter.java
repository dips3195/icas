package cancellationReport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.opencsv.CSVWriter;

public class csvWriter {

	public void csvWriterForPresenceOfData(String CHEQUE_ID, String PROPOSAL_NO, String RECEIPT_NO,
			String RECEIPT_AMOUNT, String CHANNEL_CODE, String RECON_ALLOWED, String CANCELLATION_DATE,String BRANCH_NAME,String STATE) {

		String date = getCurrentDate();

		String outputFile = System.getProperty("user.dir") + "\\Output\\Valid_Data_File_" + date + ".csv";
		// System.out.println(outputFile);

		try {
			@SuppressWarnings("deprecation")
			CSVWriter csvOutput = new CSVWriter(new FileWriter(outputFile, true), ',');

			String[] arr = { CHEQUE_ID, PROPOSAL_NO, RECEIPT_NO, RECEIPT_AMOUNT, CHANNEL_CODE, RECON_ALLOWED,
					CANCELLATION_DATE,BRANCH_NAME,STATE};

			csvOutput.writeNext(arr);

			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// This method is used to write NoDataPresent in csv
	public void csvWriterForNoDataPresent(String PROPOSAL_NO,String RECEIPT_NO,String RECEIPT_AMOUNT,
			String CHANNEL_CODE,String RECON_ALLOWED,String CANCELLATION_DATE,String BRANCH_NAME,String STATE) {
		String date = getCurrentDate();

		String outputFile = System.getProperty("user.dir") + "\\Output\\Invalid_Data_File_" + date + ".csv";

		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(outputFile, true), ',');

			String[] arr = { PROPOSAL_NO, RECEIPT_NO, RECEIPT_AMOUNT,
					CHANNEL_CODE, RECON_ALLOWED, CANCELLATION_DATE,BRANCH_NAME,STATE};

			csvOutput.writeNext(arr);

			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
		Date date = new Date();

		String time1 = dateFormat.format(date);
		return time1;
	}

	// This method is used to Clean all csv files
	public static void cleanAllCSVFiles() {

		String date = getCurrentDate();

		// System.out.println("Date" + date);
		File f = new File(System.getProperty("user.dir") + "\\Output\\Valid_Data_File_" + date + ".csv");
		File f2 = new File(System.getProperty("user.dir") + "\\Output\\Invalid_Data_File_" + date + ".csv");
		File f3 = new File(System.getProperty("user.dir") + "\\Output\\Branch_csv_" + date + ".csv");

		File file = new File(System.getProperty("user.dir") + "\\Output");

		File[] filelist = file.listFiles();

		//System.out.println("====> " + filelist.length);

		for (File file2 : filelist) {

			file2.delete();
		}
		System.out.println("All files deleted...");

		try {

			f.createNewFile();
			System.out.println("Valid_Data_File Created");
			f2.createNewFile();
			System.out.println("Invalid_Data_File File Created");
			f3.createNewFile();
			System.out.println("Branch csv File Created");

			CSVWriter csvOutput = new CSVWriter(new FileWriter(f, true), ',');
			CSVWriter csvOutput2 = new CSVWriter(new FileWriter(f2, true), ',');
			CSVWriter csvOutput3 = new CSVWriter(new FileWriter(f3, true), ',');

			String[] validFileDataArray = { "CHEQUE_ID", "PROPOSAL_NO", "RECEIPT_NO", "RECEIPT_AMOUNT", "CHANNEL_CODE",
					"RECON_ALLOWED", "CANCELLATION_DATE","BRANCH NAME","STATE" };

			String[] invalidFileDataArray = { "PROPOSAL_NO", "RECEIPT_NO", "RECEIPT_AMOUNT", "CHANNEL_CODE",
					"RECON_ALLOWED", "CANCELLATION_DATE","BRANCH NAME","STATE"};

			String[] Branch_csvDataArray = { "BRANCH_NAME", "CHEQUE_ID", "PROPOSAL_NO", "RECEIPT_NO", "RECEIPT_AMOUNT",
					"CHANNEL_CODE", };

			csvOutput.writeNext(validFileDataArray);
			csvOutput2.writeNext(invalidFileDataArray);
			csvOutput3.writeNext(Branch_csvDataArray);

			csvOutput.close();
			csvOutput2.close();
			csvOutput3.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// This method is used to delete all existing files from downloads folder
	public static void deleteAllExistingFiles() {

		File file = new File(System.getProperty("user.home") + "\\Downloads");

		File[] filelist = file.listFiles();

		// System.out.println("====> " + filelist.length);

		for (File file2 : filelist) {

			if (file2.getName().trim().contains("Cancellation_Report")) {
				file2.delete();
				System.out.println("Delete Cancellation_Report");

			}
			if (file2.getName().trim().contains("UNMAPPED_RECEIPT_DATA")) {
				file2.delete();
				System.out.println("Delete UNMAPPED_RECEIPT_DATA");
			}

		}
		System.out.println("All files deleted...");

	}

	public static String readDate() {
		String path = System.getProperty("user.dir") + "\\testdata\\Date_LoginCred_TestData.xls";

		FileInputStream fin;
		try {
			fin = new FileInputStream(path);

			HSSFWorkbook hBook = new HSSFWorkbook(fin);
			Sheet sheet = hBook.getSheet("Date");
			Row row;

			row = sheet.getRow(1);
			String str = row.getCell(0).toString();

			return str;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void csvWriterForBranchData(String BRANCH_NAME, String CHEQUE_ID, String PROPOSAL_NO, String RECEIPT_NO,
			String RECEIPT_AMOUNT, String CHANNEL_CODE) {
		String date = getCurrentDate();

		String outputFile = System.getProperty("user.dir") + "\\Output\\Branch_csv_" + date + ".csv";

		try {
			@SuppressWarnings("deprecation")
			CSVWriter csvOutput = new CSVWriter(new FileWriter(outputFile, true), ',');

			String[] arr = { BRANCH_NAME, CHEQUE_ID, PROPOSAL_NO, RECEIPT_NO, RECEIPT_AMOUNT, CHANNEL_CODE };

			csvOutput.writeNext(arr);
			// System.out.println("done");
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String readLoginDetails(String userType) {

		String path = System.getProperty("user.dir") + "\\testdata\\Date_LoginCred_TestData.xls";

		FileInputStream fin;
		FileOutputStream fout;
		String credentials = null;
		try {
			fin = new FileInputStream(path);

			HSSFWorkbook hBook = new HSSFWorkbook(fin);
			Sheet sheet = hBook.getSheet("Login_Credentials");
			Row row;
			Cell cell1;
			Cell cell2;

			int getLastRowNo = sheet.getLastRowNum() - sheet.getFirstRowNum();

			for (int i = 1; i <= getLastRowNo; i++) {

				row = sheet.getRow(i);
				String str = row.getCell(0).toString();
				if (str.equals(userType)) {

					cell1 = row.getCell(1);
					String username = cell1.toString();
					cell2 = row.getCell(2);
					String password = cell2.toString();

					credentials = username + " " + password;

					System.out.println(credentials);
					break;
				}

			}
			return credentials;

		} catch (Exception e) {
		}
		return credentials;

	}

	// // This method is used to Clean all csv files
	// public void cleanBranchCsv() {
	//
	// File f = new File(System.getProperty("user.dir") +
	// "\\Output\\Branch_csv.csv");
	//
	// try {
	// if (f.exists()) {
	// f.delete();
	// System.out.println("Valid_Data_File deleted");
	// }
	// f.createNewFile();
	// System.out.println("Branch_csv Created");
	//
	// CSVWriter csvOutput = new CSVWriter(new FileWriter(f, true), ',');
	//
	// String[] Branch_csvDataArray = { "BRANCH_NAME", "CHEQUE_ID",
	// "PROPOSAL_NO", "RECEIPT_NO", "RECEIPT_AMOUNT",
	// "CHANNEL_CODE", };
	//
	// csvOutput.writeNext(Branch_csvDataArray);
	// csvOutput.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// }

	// public static void main(String[] args) {
	// csvWriter obj = new csvWriter();
	// obj.readLoginDetails("MiFin");
}
// }
