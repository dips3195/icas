package cancellationReport;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MiFinLoginPage {

	// static String username = "B902873";
	static String username = "B73690";

	// static String username = "B901653";
	static String password = "changeit";
	static WebDriver driver;

	static String testData = "";
	static csvWriter objcsvWriter =new csvWriter();
	public static void MiFinLogin() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.get("http://10.44.0.190/mifin_recon");
		driver.get("http://10.44.0.120/mifin/miFIN/login.jsp");
		Thread.sleep(3000);

		csvWriter objcsvWriter = new csvWriter();
		testData = objcsvWriter.readLoginDetails("MiFin");

		String[] loginDetails = testData.split(" ");

		username = loginDetails[0];
		password = loginDetails[1];

		driver.findElement(By.xpath("//div//input[@name='userId']")).sendKeys(username);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div//input[@name='password']")).sendKeys(password);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@class='login_inputButtons']//button[text()='LOGIN']")).click();
	}

	public static void MISReport() {
		driver.findElement(By.xpath("//i[@id='img1000000003']")).click();
	}

	public static void ScrollDownAndClickOnUnmappedReciptData() {
		WebElement element = driver.findElement(By.xpath("//a[text()='UNMAPPED RECEIPT DATA']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", element);
		driver.findElement(By.xpath("//a[text()='UNMAPPED RECEIPT DATA']")).click();
	}

	public static void SetIdentifier() {
		driver.findElement(By.xpath("//input[@id='searchIdentifier']")).sendKeys("MiFINTest");
	}

	public static void clickOnSearch() {
		driver.findElement(By.xpath("//input[@id='SearchIt']")).click();
	}

	public static void SetDate() throws InterruptedException {
		Thread.sleep(5000);
		Alert alert = driver.switchTo().alert();
		alert.accept();

	}

	public static void clickOnSearchLink() {
		driver.findElement(By.xpath("//input[@name='search']")).click();
	}

	public static void downloadFile() throws Exception {

		// String str =
		// driver.findElement(By.xpath("//table[@class='dashRecord']//tbody//tr[2]//td[6]")).getText();

		outerloop: while (true) {

			String str = driver.findElement(By.xpath("//table[@class='dashRecord']//tbody//tr[2]//td[6]")).getText();
			System.out.println("===================> " + str);

			if (str.equals("100%")) {

				WebElement element = driver
						.findElement(By.xpath("//table[@class='dashRecord']//tbody//tr[2]//a[text()='Download']"));

				element.click();
				Thread.sleep(5000);
				System.out.println("----->Click on download");
				break outerloop;

			} else {
				Thread.sleep(60000);
				driver.navigate().refresh();
				System.out.println("----->Page refreshed");
				Thread.sleep(2000);
				clickOnSearchLink();
			}

		}

	}

	public static void clickOnDashBoardLink() {
		driver.findElement(By.xpath("//a[contains(text(),'My Dashboard')]")).click();
	}

	public static void clickOnViewer() {
		driver.findElement(By.xpath("//i[@id='img1000010344']")).click();
	}

	public static void clickOnAuthor() {
		driver.findElement(By.xpath("//i[@id='img1000010343']")).click();
	}

	public static void clickOnBATCH_UPLOAD() {
		driver.findElement(By.xpath("//i[@id='img1000010341']")).click();
	}

	public static void clickOnMAKER() {
		driver.findElement(By.xpath("//i[@id='img1000010342']")).click();
	}

	public static void clickOnCANCELLATION_UPLOAD() {
		driver.findElement(By.xpath("//div[@id='action1000010441']//a[text()='CANCELLATION_UPLOAD']")).click();
	}

	public static void clickOnViewerCANCELLATION_UPLOAD() {
		driver.findElement(By.xpath("//div[@id='action1000010440']//a[text()='CANCELLATION_UPLOAD']")).click();
	}

	public static void clickOnAuthorCANCELLATION_UPLOAD() {
		driver.findElement(By.xpath("//div[@id='action1000010439']//a[text()='CANCELLATION_UPLOAD']")).click();
	}

	public static void clickOnAuthorCheackBox() {
		driver.findElement(By.xpath("//div[@id='uploadResultsDiv']//table//tbody//input")).click();
	}

	public static void clickOnAuthorSaveButton() throws Exception {
		driver.findElement(By.xpath("//a[@id='savenew']")).click();
		Alert alert1 = driver.switchTo().alert();
		Thread.sleep(1000);
		alert1.accept();

	}

	public static void SetUploadID(String uploadID) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='uploadId']")).sendKeys(uploadID);

	}

	public static void clickOnViewerSaerch() {
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
	}

	public static void SetRemark() {
		driver.findElement(By.xpath("//textarea[@name='authorRemarks']")).sendKeys("Ok");
	}

	public static void fileUpload() throws Exception {
		String date = objcsvWriter.getCurrentDate();
		driver.findElement(By.xpath("//input[@name='dataFile']")).click();
		StringSelection cont = new StringSelection(System.getProperty("user.dir") + "\\Output\\Valid_Data_File_" + date + ".csv");
		
		Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
		c.setContents(cont, null);

		Robot r;
		try {
			Thread.sleep(10000);
			r = new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_V);
			r.keyRelease(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}

	}

	public static void SetNumberOfRecord(int counter) throws InterruptedException {

		String strCounter = String.valueOf(counter);
		driver.findElement(By.xpath("//input[@id='text_4']")).sendKeys(strCounter);
		Thread.sleep(1000);
	}

	public static String SaveFileAndGetIDNumber() throws Exception {
		driver.findElement(By.xpath("//a[@id='savenew']")).click();
		Thread.sleep(3000);
		Alert alert1 = driver.switchTo().alert();
		String uploadID = alert1.getText();
		String[] StrArr = uploadID.split(":");
		Thread.sleep(3000);
		alert1.accept();
		Thread.sleep(3000);
		alert1.accept();
		System.out.println(StrArr[1] + "===>Done");
		return StrArr[1].trim();
	}

	public static void logout() throws Exception {
		driver.findElement(By.xpath("//img[@class='userr']")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		Thread.sleep(3000);
		closeBrowser();
	}

	public static void ViewerStatus() throws Exception {

		outerloop: while (true) {

			String str = driver.findElement(By.xpath("(//div[@id='uploadResultsDiv']//table//tbody//tr[2]//td[4])[1]"))
					.getText();
			System.out.println("===================> " + str);

			if (str.equals("COMPLETE VALID FILE") || str.equals("PARTIAL VALID FILE")) {

				WebElement element = driver.findElement(By.xpath("//a[contains(text(),'My Dashboard')]"));

				element.click();
				Thread.sleep(5000);
				System.out.println("----->Click on My Dashboard");
				break outerloop;

			} else {
				Thread.sleep(60000);
				driver.navigate().refresh();
				System.out.println("----->Page refreshed");
				Thread.sleep(2000);

			}

		}

	}

	public static void ViewerFinalStatus() throws Exception {

		outerloop: while (true) {

			String str = driver.findElement(By.xpath("(//div[@id='uploadResultsDiv']//table//tbody//tr[2]//td[4])[1]"))
					.getText();
			System.out.println("===================> " + str);

			if (str.equals("ALL VALID RECORDS UPLOADED")) {

				WebElement element = driver.findElement(By.xpath("//a[contains(text(),'My Dashboard')]"));

				element.click();
				Thread.sleep(5000);
				System.out.println("----->Click on My Dashboard");
				break outerloop;

			} else {
				Thread.sleep(60000);
				driver.navigate().refresh();
				System.out.println("----->Page refreshed");
				Thread.sleep(2000);

			}

		}

	}

	public static void closeBrowser() {
		driver.quit();
	}
	
	public static void ShowCompletedMessage() {
		
		JOptionPane.showMessageDialog(null, "All Operations successfully done...");
	}

}
