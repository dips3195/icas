package cancellationReport;

public class CancellationClass {

	static int counter;

	public static void main(String[] args) throws Exception {
		csvWriter.deleteAllExistingFiles();
		// Download Cancellation_Report from epaisa
		String date = csvWriter.readDate();

		System.out.println("date-------------> " + date);
		csvWriter.cleanAllCSVFiles();

		EpaisaLoginPage.ePaisaLogin();
		EpaisaLoginPage.ClickOnReports();
		EpaisaLoginPage.ClickOnCancellationReports();
		EpaisaLoginPage.SelectFromDate(date);
		EpaisaLoginPage.SelectToDate(date);
		EpaisaLoginPage.ClickOnDownload();
		EpaisaLoginPage.Logout();

		// Download UNMAPPED RECEIPT DATA from MiFin
		MiFinLoginPage.MiFinLogin();
		MiFinLoginPage.MISReport();
		MiFinLoginPage.ScrollDownAndClickOnUnmappedReciptData();
		MiFinLoginPage.SetIdentifier();
		MiFinLoginPage.clickOnSearch();
		MiFinLoginPage.SetDate();
		MiFinLoginPage.clickOnSearchLink();
		MiFinLoginPage.downloadFile();
		MiFinLoginPage.logout();

		// Generate report
		CancellationReportCsv.Cancellation_Report_csv();
		counter = CancellationReportCsv.getCounteOfValidData();
		System.out.println(counter + "------------->");

		MiFinLoginPage.MiFinLogin();
		MiFinLoginPage.clickOnBATCH_UPLOAD();
		MiFinLoginPage.clickOnMAKER();
		MiFinLoginPage.clickOnCANCELLATION_UPLOAD();
		MiFinLoginPage.fileUpload();
		MiFinLoginPage.SetNumberOfRecord(counter);
		String uploadId = MiFinLoginPage.SaveFileAndGetIDNumber();
		MiFinLoginPage.clickOnDashBoardLink();

		MiFinLoginPage.clickOnBATCH_UPLOAD();
		MiFinLoginPage.clickOnViewer();
		MiFinLoginPage.clickOnViewerCANCELLATION_UPLOAD();
		MiFinLoginPage.SetUploadID(uploadId);
		MiFinLoginPage.clickOnViewerSaerch();
		MiFinLoginPage.ViewerStatus();

		MiFinLoginPage.clickOnBATCH_UPLOAD();
		MiFinLoginPage.clickOnAuthor();
		MiFinLoginPage.clickOnAuthorCANCELLATION_UPLOAD();
		MiFinLoginPage.clickOnAuthorCheackBox();
		MiFinLoginPage.SetRemark();
		MiFinLoginPage.clickOnAuthorSaveButton();
		MiFinLoginPage.clickOnDashBoardLink();

		MiFinLoginPage.clickOnBATCH_UPLOAD();
		MiFinLoginPage.clickOnViewer();
		MiFinLoginPage.clickOnViewerCANCELLATION_UPLOAD();
		MiFinLoginPage.SetUploadID(uploadId);
		MiFinLoginPage.clickOnViewerSaerch();
		MiFinLoginPage.ViewerFinalStatus();
		MiFinLoginPage.logout();
		MiFinLoginPage.closeBrowser();

		// Generate report with branch name
		Finone.csvBranchDataWriter();

		MiFinLoginPage.ShowCompletedMessage();
	}

}
