package cancellationReport;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;

public class Finone {
	// public static void main(String[] args) throws Exception {
	// Finone f = new Finone();
	// f.csvBranchDataWriter();
	// }

	public static String getBranchNameFromValidFile(String branch) {

		String filePath = System.getProperty("user.dir") + "\\testdata\\Branch.xlsx";

		String branchName = "";

		XSSFWorkbook wbook;
		XSSFSheet sheet;
		FileInputStream fin;
		Row row;

		try {

			fin = new FileInputStream(filePath);

			wbook = new XSSFWorkbook(fin);
			sheet = wbook.getSheet("Export Worksheet");

			for (int i = 1; i < sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);

				Cell cell = row.getCell(1);
				String str = cell.toString();

				if (str.equals(branch)) {

					branchName = row.getCell(2).toString();
					break;

				}

			}

		} catch (

		Exception e) {
			e.printStackTrace();

		}
		return branchName;
	}

	public static void csvBranchDataWriter() throws IOException {

		csvWriter obj = new csvWriter();

		// delete existing file and create new Branch.csv file
		// obj.cleanBranchCsv();
		String date = obj.getCurrentDate();
		String csvFile = System.getProperty("user.dir") + "\\Output\\Valid_Data_File_" + date + ".csv";

		try {

			FileReader filereader = new FileReader(csvFile);

			CSVReader csvReader = new CSVReader(filereader);
			String[] nextRecord;

			int i = 0;
			String CHEQUE_ID = "";
			String PROPOSAL_NO = "";
			String RECEIPT_NO = "";
			String RECEIPT_AMOUNT = "";
			String CHANNEL_CODE = "";

			String branchNameInShort = "";
			String BRANCH_NAME = "";

			while ((nextRecord = csvReader.readNext()) != null) {
				i++;

				if (i > 1) {

					CHEQUE_ID = nextRecord[0];

					PROPOSAL_NO = nextRecord[1];
					RECEIPT_NO = nextRecord[2];
					RECEIPT_AMOUNT = nextRecord[3];
					CHANNEL_CODE = nextRecord[4];

					branchNameInShort = PROPOSAL_NO.substring(3, 6);

					BRANCH_NAME = getBranchNameFromValidFile(branchNameInShort);

					obj.csvWriterForBranchData(BRANCH_NAME, CHEQUE_ID, PROPOSAL_NO, RECEIPT_NO, RECEIPT_AMOUNT,
							CHANNEL_CODE);

					CHEQUE_ID = "";
					PROPOSAL_NO = "";
					RECEIPT_NO = "";
					RECEIPT_AMOUNT = "";
					CHANNEL_CODE = "";
					BRANCH_NAME = "";
					branchNameInShort = "";

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
